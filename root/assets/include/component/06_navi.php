<?php /*========================================
navi
================================================*/ ?>
<div class="c-dev-title1">navi</div>

<?php /*----------------------------------------*/ ?>
<div class="c-dev-title2">c-navi1</div>
<div class="c-icon">
    <span class="c-icon__line"></span>
    <span class="c-icon__line"></span>
    <span class="c-icon__line"></span>
</div>
<br>
<ul class="c-navi1">
    <li class="sp-only">
        <div class="c-navi1__form">
            <form>
                <input type="text" name="" value="" placeholder="キーワード">
                <input type="submit" value="">
            </form>
        </div>
    </li>
    <li>
        <p class="c-navi1__item">製品一覧</p>
        <div class="c-navi1__child">
            <div class="c-navi1__child__content">
                <div class="sp-only">
                    <p><a href="#">キャンペーン</a></p>

                </div>
                <div>
                    <p><a href="#">コードレス掃除機</a></p>
                    <p><a href="#">V11 シリーズ</a></p>
                    <p><a href="#">V10 シリーズ</a></p>
                    <p><a href="#">V8 シリーズ</a></p>
                    <p><a href="#">V7 シリーズ</a></p>
                    <p><a href="#">V6 シリーズ</a></p>
                </div>
                <div>
                    <p><a href="#">ハンディクリーナー</a></p>
                    <p><a href="#">V7 シリーズ</a></p>
                    <p><a href="#">V6 シリーズ</a></p>
                    <p><a href="#">DC61 シリーズ</a></p>
                    <p><a href="#">DC34/ 43 シリーズ</a></p>

                </div>
                <div>
                    <p><a href="#">キャニスター型掃除機</a></p>
                    <p><a href="#">Dyson Ball/ V4 Digital<br class="pc-only">シリーズ</a></p>
                    <p><a href="#">DC48 シリーズ</a></p>
                </div>
                <div>
                    <p><a href="#">ロボット掃除機</a></p>
                    <p><a href="#">Dyson 360</a></p>
                </div>

                <div>
                    <p><a href="#">空調家電</a></p>
                    <p><a href="#">空気清浄機能付扇風機</a></p>
                    <p><a href="#">空気清浄機能付ファンヒーター</a></p>
                    <p><a href="#">加湿器</a></p>
                </div>
                <div>
                    <p><a href="#">ヒーター・扇風機</a></p>
                    <p><a href="#">扇風機</a></p>
                    <p><a href="#">ヒーター&amp;扇風機</a></p>
                </div>
                <div>
                    <p><a href="#">照明</a></p>
                    <p><a href="#">Lightcycle</a></p>
                    <p><a href="#">CSYS™タスクライト</a></p>
                </div>
                <div>
                    <p><a href="#">ツール・付属品</a></p>
                    <p><a href="#">掃除機ツール・付属品</a></p>
                    <p><a href="#">空調家電ツール・付属品</a></p>
                </div>
            </div>
        </div>
    </li>
    <li class="pc-only">
        <p class="c-navi1__item"><a href="#">キャンペーン</a></p>
    </li>
    <li>
        <p class="c-navi1__item"><a href="#">決済・配送・返品</a></p>
    </li>
    <li class="sp-only">
        <div class="c-navi1__info">
            <h3 class="c-navi1__tit">お問い合わせ </h3>
            <a href="#" class="c-navi1__bnt">お問い合わせフォーム</a>
            <div class="c-navi1__tel"><img src="/root/assets/img/common/icon_tel.gif" alt="">0120-295-731</div>
            <p class="c-navi1__text">受付時間<br>午前9:00-午後5:30<br>(年末年始等、特別休暇除く)</p>
            <h3 class="c-navi1__tit">製品保証</h3>
            <a href="#" class="c-navi1__bnt">オンライン登録</a>
            <p class="c-navi1__note">※Dyson公式サイトへリンクします</p>
        </div>
    </li>
</ul>